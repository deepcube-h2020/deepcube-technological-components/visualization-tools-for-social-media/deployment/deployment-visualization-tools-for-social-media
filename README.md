# Deployment Visualization tools for social media

Deploy Visualization tools for social media on the DeepCube Platform

## Getting started

For both déployment

1. `source vault.fish`
2. Create secrets with `ansible-vault create secret_vars/all.yml  --vault-password-file "./.vault_password"`
   
### SWARM Deployment


1. `ansible-playbook -i hosts/all.yml deploy.yml --vault-password-file "./.vault_password"`

### Kubernetes Cluster Deployment


1. `ansible-playbook -i hosts/all.yml deploy_on_k8s.yml --vault-password-file "./.vault_password"`